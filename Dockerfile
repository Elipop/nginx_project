# Возьму оригинальный образ Nginx.
FROM nginx

#Копирую свою страничку
COPY html/index.html /usr/share/nginx/html/

#COPY conf/nginx.conf /etc/nginx/nginx.conf не буду конфиг перекидывать
#RUN echo "Hello Eremin_Anton" > /usr/share/nginx/index.html решил перекину свою страничку

#Домашняя директория
WORKDIR /etc/nginx

# По умолчанию порты
EXPOSE 80

# Запуск
ENTRYPOINT ["nginx", "-g", "daemon off;"]